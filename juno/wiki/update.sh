#!/bin/bash

echo Script para actualizar MediaWiki en servidores de wikisp.org
echo Actualizando repositorios del sistema

#sudo apt update -y
#sudo apt upgrade -y

echo Listo! Descargando nueva versión de MediaWiki 1.42.1

curl -O https://releases.wikimedia.org/mediawiki/1.42/mediawiki-1.42.1.zip

echo Sacando del paquete a MediaWiki 1.42.1

unzip mediawiki-*.zip

echo Cambiando el nombre de la carpeta. Moviendo archivos.

mv mediawiki-1.42.1 wiki2
cp wikisp-infrastructure/juno/wiki/LocalSettings.php wiki2/LocalSettings.php

sudo -s
mv wiki2 /var/www/wikisp/wiki2
cp /var/www/wikisp/wiki/private/PrivateSettings.php /var/www/wikisp/wiki2/private/PrivateSettings.php
cp /var/www/wikisp/wiki/.htaccess /var/www/wikisp/wiki2/.htaccess
cp /var/www/wikisp/wiki/{Part_of_WSP.svg,apple-touch-icon.png,favicon.ico,wikisp-icon.svg,wikisp-logo.svg,
wikisp-wordmark.svg} /var/www/wikisp/wiki2/resources/assets

echo Moviéndonos a carpeta extensiones y descargando
cd /var/www/wikisp/wiki2/extensions
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/Echo --branch REL1_42
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/Translate --branch REL1_42
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/UserMerge --branch REL1_42
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/CodeEditor --branch REL1_42
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/CodeMirror --branch REL1_42
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/Babel --branch REL1_42
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/CharInsert --branch REL1_42
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/intersection --branch REL1_42
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/TemplateStyles --branch REL1_42
git clone https://github.com/wikimediasp/MediaWiki-Counter.git Counter
git clone https://github.com/StarCitizenTools/mediawiki-extensions-TabberNeue.git TabberNeue
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/cldr --branch REL1_42
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/CleanChanges --branch REL1_42
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/PluggableAuth --branch REL1_42
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/SecurePoll --branch REL1_42
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/UniversalLanguageSelector --branch REL1_42
#git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/WSOAuth
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/PageForms.git --branch REL1_42
git clone https://gerrit.wikimedia.org/r/mediawiki/extensions/timeline --branch REL1_42
git clone https://github.com/StarCitizenTools/mediawiki-extensions-ShortDescription.git ShortDescription

echo Eliminando extensiones innecesarias
rm -rf AbuseFilter CiteThisPage ConfirmEdit ImageMap PageImages MultimediaViewer SecureLinkFixer Nuke OATHAuth PdfHandler ReplaceText TextExtracts TitleBlacklist

echo Usando composer install

cd /var/www/wikisp/wiki2/extensions/PluggableAuth
composer install --no-dev

#cd /var/www/wikisp/wiki2/extensions/WSOAuth 
#composer install --no-dev
cp /var/www/wikisp/wiki/extensions/WSOAuth /var/www/wikisp/wiki2/WSOAuth

echo Descargando skin principal
cd skins
git clone https://github.com/StarCitizenTools/mediawiki-skins-Citizen.git Citizen

echo Estamos preparados. Por favor, bloquea la base de datos. 
sleep 5s 
echo Cambiando lugares
mv /var/www/wikisp/wiki /var/www/wikisp/oldwiki
mv /var/www/wikisp/wiki2 /var/www/wikisp/wiki

echo Lugares cambiados
echo Iniciando actualización de base de datos

php /var/www/wikisp/wiki/maintenance/update.php

echo El script de actualización ha finalizado sus labores.
echo Por favor revisa que todo esté correcto. De otro modo NO elimines oldwiki.
echo ¡Gracias!